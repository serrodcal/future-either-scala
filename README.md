Future-Either PoC

[![pipeline status](https://gitlab.com/serrodcal/future-either-scala/badges/master/pipeline.svg)](https://gitlab.com/serrodcal/future-either-scala/commits/master)

This project contains of a proof of concept about Monads,
Future, Either and so on in order to learn about this 
new concepts from functional programming in Scala.

## Getting Started

### Prerequisites

Install [Maven](https://maven.apache.org/download.cgi), [Java 8+](https://www.java.com/es/download/) 
and [Scala](https://www.scala-lang.org/download/), then clone this repository in your local.

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Scala](https://www.scala-lang.org/) - Language

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Sergio Rodríguez Calvo** - *Developer* - [Github](https://github.com/serrodcal)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [logicaalternativa](https://github.com/logicaalternativa) for introducing me to this world with help, courses and so on.
