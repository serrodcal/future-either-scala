package com.serrodcal.poc.scala

import cats.data.EitherT
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object Main extends App {

  import scala.concurrent.ExecutionContext.Implicits.global
  import cats.implicits._

  // Add cats-core to work with EitherT[F[_], A, B] instead of Future[Either[A, B]

  def arcsin(x: Double) : EitherT[Future, String, Double] = x match {
      case x if x >= -1 && x <= 1 => {
        val fvalue: Future[Either[String, Double]] = Future.successful(Right(scala.math.asin(x)))
        EitherT(fvalue)
      }
      case _ => {
        val fvalue: Future[Either[String, Double]] = Future.successful(Left("Arcsin can not operate a value outside [-1,1] range"))
        EitherT(fvalue)
      }
  }

  def sqrt(x: Double) : EitherT[Future, String, Double] = x match {
      case x if x != 0 => {
        val fvalue: Future[Either[String, Double]] = Future.successful(Right(scala.math.sqrt(x)))
        EitherT(fvalue)
      }
      case _ => {
        val fvalue: Future[Either[String, Double]] = Future.successful(Left("SQRT can not operate a less than zero value"))
        EitherT(fvalue)
      }
  }

  def inverse(x: Double) : EitherT[Future, String, Double] = x match {
      case x if x > 0 => {
        val fvalue: Future[Either[String, Double]] = Future.successful(Right(1/x))
        EitherT(fvalue)
      }
      case _ => {
        val fvalue: Future[Either[String, Double]] = Future.successful(Left("Inverse can not operate a zero value"))
        EitherT(fvalue)
      }
  }

  val input: Double = 1

  val result: EitherT[Future, String, Double] = for {
    r1 <- arcsin(input)
    r2 <- sqrt(r1)
    r3 <- inverse(r2)
  } yield r3

  val value: Future[Either[String, Double]] = result.value

  val futureResult: Either[String, Double] = Await.result(value, 1 second)

  futureResult match {
    case Right(x) => println(x)
    case Left(x) => println(x)
  }

}
